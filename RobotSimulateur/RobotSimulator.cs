﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotSimulateur
{
    public class RobotSimulator
    {
        public Direction Direction { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public RobotSimulator(Direction direction, int x, int y)
        {
            this.Direction = direction;
            this.X = x;
            this.Y = y;
        }

        public void Move(string command)
        {
            foreach(char c in command)
            {
                switch (c)
                {
                    case 'R':
                        switch (Direction)
                        {
                            case Direction.North:
                                this.Direction = Direction.East;
                                break;
                            case Direction.South:
                                this.Direction = Direction.West;
                                break;
                            case Direction.East:
                                this.Direction = Direction.South;
                                break;
                            case Direction.West:
                                this.Direction = Direction.North;
                                break;
                        }
                        break;
                    case 'L':
                        switch (Direction)
                        {
                            case Direction.North:
                                this.Direction = Direction.West;
                                break;
                            case Direction.South:
                                this.Direction = Direction.East;
                                break;
                            case Direction.East:
                                this.Direction = Direction.North;
                                break;
                            case Direction.West:
                                this.Direction = Direction.South;
                                break;
                        }
                        break;
                    default:
                        switch (Direction)
                        {
                            case Direction.North:
                                this.Y += 1;
                                break;
                            case Direction.South:
                                this.Y -= 1;
                                break;
                            case Direction.East:
                                this.X += 1;
                                break;
                            case Direction.West:
                                this.X -= 1;
                                break;
                        }
                        break;
                }
            }
        }

    }
}
